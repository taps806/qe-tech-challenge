

describe("Get weather ", () => {
 //Decalare Variabled 
    const latitude = 145.5;
    const longitude = 69.45;
    const post_code = 2005;
    it("Call base on longitude and latitude", () => {

        /*Sending GET request to get weather
        Params: lat: latitude, lon: longitude, key : api_key*/
        cy.request({
            qs:
            {
                lat: latitude,
                lon: longitude,
                key: "c6a5d6bc5a3a420497d00532e08b53fa"
            },

            url: '/current',
            log: true

        })
         //  Checking response equal 200OK and excrating  the item 0 
           .then((resp) => {
            expect(resp.status).to.eq(200)

           }).its('body').its('data').its('0')

    })

    it("Call base postcode", () => {
    /*Sending GET request to get weather
    Params: postcal_code: post code, key : api_key*/
        cy.request({
            qs:
            {
                postal_code: post_code,
                key: "c6a5d6bc5a3a420497d00532e08b53fa"
            },

            url: '/current',
            log: true



        })
 //  Checking response equal 200OK and excrating  the item 0 
            .then((resp) => {
                expect(resp.status).to.eq(200)

            }).its('body').its('data').its('0')

    })
})

