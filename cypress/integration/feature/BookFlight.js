describe('Book a hotel,flight or tour', () => {

    const adultnumber = 4;

    Cypress.on('uncaught:exception', (err, runnable) => {
      
        return false
    })

    it('book a flight', () => {
        cy.visit('https://www.phptravels.net/home');
        cy.get('.dropdown-login').click().

            get('.dropdown-menu > div > .active').click();

        cy.get('form').within(($form) => {
            
            cy.get('input[type="email"]').click({ force: true }).type('user@phptravels.com')
            cy.get('input[type="password"]').click({ force: true }).type('demouser')
            cy.get('.loginbtn').click();

            cy.location('pathname', { timeout: 60000 })
                .should('include', '/account/');

        })



        cy.get('*[title="home"]').click();


        //Book a flight 


        cy.get('a[data-name=flights]').click();


        cy.get('#s2id_location_from > .select2-choice').type("Sydney").get(':nth-child(4) > .select2-result-label').click();

        cy.get('#s2id_location_to > .select2-choice > .select2-chosen').type("New York").get(':nth-child(2) > .select2-result-label').click();


        cy.get('input[id=FlightsDateStart]').click();


        

        cy.get('#datepickers-container > div:nth-child(8) > div > div > div.datepicker--cells.datepicker--cells-days > div:nth-child(10)').click();
      
       
        
        cy.get('form[name=flightmanualSearch]').submit()
       

        cy.get(":nth-child(1) > .theme-search-results-item > .theme-search-results-item-preview > [data-gutter='20'] > form.row > :nth-child(6) > .theme-search-results-item-book > .btn").click();
        cy.get('#name').type("Teny");
        cy.get('#surname').type("Peter");
        cy.get('#email').type("teny@gmail.com")
        cy.get('#phone').type("0470404023")
        cy.get('#birthday').type("1993-03-03")
        cy.get('#cardno').type("ASP23212")
        cy.get('#expiration').type("2024-03-03")
        cy.get('.select2-choice').click()
        cy.get(':nth-child(13) > .select2-result-label').click()
        cy.get('#cardtype').select("Visa")
        cy.get('#card-number').type("123456789")
        cy.get('#expiry-month').select("Dec (12)");
        cy.get('#expiry-year').select("2030");
        cy.get('#cvv').type("123")
        cy.get('.custom-checkbox > .custom-control-label').click();
        

        cy.get("form[name=ticketBookingForm]").submit();
      
    })




})